import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Meta from 'vue-meta';
import VueCarousel from 'vue-carousel';

Vue.use(VueCarousel);
Vue.use(Meta);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
