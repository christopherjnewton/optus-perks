import Vue from 'vue'
import Router from 'vue-router'
import NotFound from './views/NotFound.vue'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Support from './views/Support.vue'
import Terms from './views/Terms.vue'
import faq from './views/FAQ.vue'
import perks from './views/Perks.vue'
import perks1 from './views/Perks1.vue'
import perks3 from './views/Perks3.vue'
import Partners from './views/Partners.vue'
import supplierbuy from './views/Supplierbuy.vue'
import events from './views/Events.vue'
import competition from './views/Competition.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: "*",
            name: "NotFound",
            component: NotFound
        },
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/About',
            name: 'about',
            component: About
        },
        {
            path: '/Support',
            name: 'support',
            component: Support
        },
        {
            path: '/Terms',
            name: 'terms',
            alias: '/productterms',
            component: Terms
        },
        {
            path: '/FAQ',
            name: 'faq',
            component: faq
        },
        {
            path: '/perks',
            name: 'perks',
            component: perks
        },
        {
            path: '/perks1',
            name: 'perks1',
            component: perks1
        },
        {
            path: '/perks3',
            name: 'perks3',
            component: perks3
        },
        {
            path: '/Partners',
            name: 'partners',
            component: Partners
        },
        {
            path: '/supplierbuy',
            name: 'supplierbuy',
            component: supplierbuy
        },
        {
            path: '/events',
            name: 'events',
            component: events
        },
        {
            path: '/competition',
            name: 'competition',
            component: competition
        },
        {
            path: '/Hoyts',
            redirect: to => {
                window.location = 'https://www.optus.com.au/customercentre/rewards/'
            }
        }
 
    ]
})
